
exports.up = (knex) =>
  knex.schema
    .createTable('commerces', (table) => {
      /** Fields the */
      table.increments('id')
      table.string('api_key', 32).notNullable()
      table.string('name', 50).notNullable()
      table.string('legal_name', 100).notNullable()
      table.string('rfc', 13).notNullable()
      table.string('address', 100).nullable()
      table.string('state', 20).nullable()
      table.string('country', 2).defaultTo('MX')
      table.string('zip_code', 5).nullable()
      table.timestamps(true)

      /** Indexes */
    })

exports.down = (knex) =>
  knex.schema
    .dropTable('commerces')
