
exports.up = (knex) =>
  knex.schema
    .createTable('customers', (table) => {
      /** Fields the */
      table.increments('id')
      table.string('first_name', 20).notNullable()
      table.string('last_name', 20).notNullable()
      table.string('email', 255).notNullable()
      table.string('phone', 13).nullable()
      table.boolean('full_verified').defaultTo(false)
      table.boolean('id_verified').defaultTo(false)
      table.boolean('photo_verified').defaultTo(false)
      table.boolean('gov_verified').defaultTo(false)
      table.boolean('buro_verified').defaultTo(false)
      table.timestamps(true)

      /** Indexes */
    })

exports.down = (knex) =>
  knex.schema
    .dropTable('customers')
