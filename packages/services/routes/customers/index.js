'use strict'

module.exports = async function (fastify, _opts) {
    fastify.post('/', async (request, reply) => {
        const { firstName, lastName, email, phone } = request.body

        try {
            const client = await fastify.pg.connect()
            const { rows } = await client.query(
                'insert into customers(first_name, last_name, email, phone) values ($1, $2, $3, $4) returning *',
                [firstName, lastName, email, phone]
            )
            client.release()

            reply.code(201)
            return rows[0]
        } catch (err) {
            throw err
        }
    })

    fastify.get('/', async (_request, _reply) => {
        try {
            const client = await fastify.pg.connect()
            const { rows } = await client.query('select * from customers')
            client.release()

            return rows
        } catch (err) {
            throw err
        }
    })

    fastify.get('/:id', async (request, reply) => {
        try {
            const client = await fastify.pg.connect()
            const { rows } = await client.query('select * from customers where id = $1', [request.params.id])
            client.release()

            if (!rows.length) {
                reply.code(404)
                return {}
            }

            return rows[0]
        } catch (err) {
            throw err
        }
    })

    // TODO: FIXME
    fastify.patch('/:id', async (request, reply) => {
        try {
            const client = await fastify.pg.connect()
            await client.query('update customers  where id = $1', [request.params.id])

            reply.code(204)
            return {}
        } catch (err) {
            throw err
        }
    })

    fastify.delete('/:id', async (request, reply) => {
        try {
            const client = await fastify.pg.connect()
            await client.query('delete from customers where id = $1', [request.params.id])

            reply.code(204)
            return {}
        } catch (err) {
            throw err
        }
    })
}