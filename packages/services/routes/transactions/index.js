'use strict'

const { postTransaction } = require('../../schemas/index')

module.exports = async function (fastify, _opts) {
    fastify.post('/', postTransaction, async (request, reply) => {
        const { customer, isApproved, gateway, deviceSessionId, userAgent, amount, currency, cardToken } = request.body

        try {
            const client = await fastify.pg.connect()

            const { rows } = await client.query(
                'insert into transactions(customer_email, customer_phone, customer_ip, is_approved, gateway, device_session_id, user_agent, amount, currency, card_token) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) returning id',
                [customer?.email, customer?.phone, customer?.ip, isApproved, gateway, deviceSessionId, userAgent, amount, currency, cardToken]
            )

            reply.code(201)
            return rows[0]
        } catch (err) {
            throw err
        }
    })
}
