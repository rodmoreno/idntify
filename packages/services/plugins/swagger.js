'use strict'

const fp = require('fastify-plugin')

/**
 * This plugins adds support for Swagger/OpenAPI documentation
 *
 * @see https://github.com/fastify/fastify-swagger
 */
module.exports = fp(async function (fastify, _opts) {
  fastify.register(require('fastify-swagger'), {
    exposeRoute: true,
    routePrefix: '/docs',
    openapi: {
      info: {
        title: 'IDntify API',
        description: 'IDntify documentation to verify payments orders',
        version: '1.0.0'
      },
      externalDocs: {
        url: 'https://idntify.io',
        description: 'Find more info here'
      },
      servers: [
        {
          url: 'https://api.staging.idntify.io',
          description: 'Staging server'
        },
        {
          url: 'https://api.idntify.io',
          description: 'Production server'
        }
      ],
      components: {
        securitySchemes: {
          apiKey: {
            type: 'apiKey',
            name: 'apiKey',
            in: 'header'
          }
        }
      },
      uiConfig: {
        docExpansion: 'full',
        deepLinking: false
      },
      staticCSP: true,
      transformStaticCSP: (header) => header
    }
  })
})
